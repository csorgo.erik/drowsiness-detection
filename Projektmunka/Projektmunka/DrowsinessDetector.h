#pragma once
#include <opencv2/core/core.hpp>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing.h>

#define EAR_THRESHOLD 0.20
#define EYE_CONS_FRAME 72 // ~2sec
#define FRAME_SKIPS 1
#define MAX_BLINKS_PER_MIN 15
#define MIN_BLINKS_PER_MIN 10
#define MAR_THRESHOLD 0.25

using namespace cv;
using namespace dlib;
using namespace std;

class DrowsinessDetector
{
public:
	DrowsinessDetector();
	void start();
	void start(const char* file);
	void drawPoints(Mat& image, full_object_detection landmarks);
	float calcLeftEAR(full_object_detection landmarks);
	float calcRightEAR(full_object_detection landmarks);
	float calcMAR(full_object_detection landmarks);
	void displayStat(Mat& img, int blinks, float ear, float mar);
	void warning(const char* msg);
private:
	frontal_face_detector detector;
	shape_predictor predictor;
	unsigned int close_counter;
	unsigned int blinks;
	unsigned int total_frames;
};

